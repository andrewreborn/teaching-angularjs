// Styles
import "../scss/style.scss";

// Dependencies
import "jquery";
import "bootstrap/dist/js/bootstrap";
import * as angular from "angular";

const appModule = angular.module("App", []);

appModule.controller("HomeController", function HomeController() {
  this.title = "My first AngularJS app!";
  this.subtitle =
    "This framework looks awesome, but I've still got a lot to learn...";
});
